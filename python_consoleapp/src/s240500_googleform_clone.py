"""
create form  ref https://developers.google.com/forms/api/guides/create-form-quiz#create_a_new_form

ref https://developers.google.com/forms/api/guides/create-form-quiz#python_1
"""
from googleapiclient.discovery import build
from python_consoleapp.src.service.google_client._ import do_auth_n_get_tokenfile


creds = do_auth_n_get_tokenfile()

service = build('drive', 'v3', credentials=creds)

# Call the Drive v3 API to copy
copyfr_file_id = '1aRkIi3ZckpfG3XGMElvOJgQ8j-CVZVKGtUbxXA2Ryd0'  # example id
copyto_file_json = {
  'title': 'testcopy',
  'name':  'testcopy',
}
results = service.files().copy(fileId=copyfr_file_id, body=copyto_file_json).execute()
'''
TODO
googleapiclient.errors.HttpError: <HttpError 403 when requesting https://www.googleapis.com/drive/v3/files/1aRkIi3ZckpfG3XGMElvOJgQ8j-CVZVKGtUbxXA2Ryd0/copy?alt=json returned "Request had insufficient authentication scopes.". Details: "[{'message': 'Insufficient Permission', 'domain': 'global', 'reason': 'insufficientPermissions'}]">
'''
print(results)

_='''
{'kind': 'drive#file', 'id': '1EoEsUx8z1pWolBG8jq7VejRKvPSiETfWWCSuBPZmIhs', 'name': 'Copy of Untitled form', 'mimeType': 'application/vnd.google-apps.form'}
'''
