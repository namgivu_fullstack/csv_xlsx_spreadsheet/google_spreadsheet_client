from python_consoleapp.src.service.google_client._ import do_auth_n_get_tokenfile, write_range

values = [
  ['a', 'bb'],
  ['c', 'dd'],
]

print(write_range(
  values      = values,
  sheet_id    = '12RoMSEtnY7Xlq1i510mWRrQuyECiZF7PCwNUCTPdYag',
  cell_range  = 'Sheet 2!B2:C',
  credentials = do_auth_n_get_tokenfile(),
))

print(write_range(
  values = [
    ['x', 'yy'],
    ['z', 'tt'],
  ],
  sheet_id    = '12RoMSEtnY7Xlq1i510mWRrQuyECiZF7PCwNUCTPdYag',
  cell_range  = 'Sheet 2!B2:C',
  credentials = do_auth_n_get_tokenfile(),
))
