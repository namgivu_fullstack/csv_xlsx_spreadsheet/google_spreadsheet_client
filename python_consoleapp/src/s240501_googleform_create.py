"""
create form  ref https://developers.google.com/forms/api/guides/create-form-quiz#create_a_new_form

ref https://developers.google.com/forms/api/guides/create-form-quiz#python_1
"""
from pathlib import Path
#
from apiclient import discovery
from httplib2 import Http
from python_consoleapp.src.service.google_client._ import do_auth_n_get_tokenfile


SCOPES        = 'https://www.googleapis.com/auth/drive'
DISCOVERY_DOC = 'https://forms.googleapis.com/$discovery/rest?version=v1'

creds = do_auth_n_get_tokenfile()

#region create new form
form_service = discovery.build(
  'forms', 'v1',
  http=creds.authorize(Http()), discoveryServiceUrl=DISCOVERY_DOC, static_discovery=False
)
'''
TODO
AttributeError: 'Credentials' object has no attribute 'authorize'
'''

formtitle="testcreateform"
form_body = {
  "info": {
    "title": formtitle,  #NOTE this is form title, and NOT the googledrive filename of that form; filename to be 'Untitled form'
  }
}
created_form_r = form_service.forms().create(body=form_body).execute()
print('\n--- created_form_r')
print(created_form_r)

_='''
Authentication successful.
{
  'formId': '1aRkIi3ZckpfG3XGMElvOJgQ8j-CVZVKGtUbxXA2Ryd0', 
  'info': {
    'title': 'testcreateform', 
    'documentTitle': 'Untitled form'
  }, 
  'revisionId': '00000002', 
  'responderUri': 'https://docs.google.com/forms/d/e/1FAIpQLSfEHcYIsFGrmArV7E9KtAa53T6KTVbXEyv2VK_ObUloD5v5xA/viewform'
}

responderUri ie the url to view the form - NOTE id in this url is NOT form id
#     //                forms/d/e/xxx
https://docs.google.com/forms/d/e/1FAIpQLSdvhvILZ6aYoC3Yv_6OIOI8Bqkm2KoIyhl_tU5sdiLqzC7W3Q/viewform?usp=sf_link
'''
#endregion create new form

#region name the form's googledrive filename as same :formtitle
from googleapiclient.discovery import build
drive_service = build('drive', 'v3', credentials=creds)
r = drive_service.files().update(fileId=created_form_r['formId'], body={"name": formtitle}).execute()
print('\n\n--- name form')
print(r)
#endregion name the form's googledrive filename as same :formtitle
