from pathlib import Path
import io
#
# googleclient auth
from google.oauth2.credentials import Credentials
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
#
# googleclient service/client-loading @ spreadsheet, drive
from googleapiclient.discovery import build
#
# ggdrive
from googleapiclient.http import MediaIoBaseDownload


def do_auth_n_get_tokenfile():
  SH    = Path(__file__).parent

  clientsecret_json_f = credentials_json_f = SH/'_env/clientsecret.json'
  assert credentials_json_f.exists(), f'ERROR `credential json file` aka `gcp client secret json` is required at {credentials_json_f}'

  token_json_f = SH/'token.json'  # previous authenticated token file stored here

  SCOPES = [
    # all scopes  ref https://developers.google.com/sheets/api/scopes
    'https://www.googleapis.com/auth/spreadsheets',
    'https://www.googleapis.com/auth/drive',
  ]

  #region try previous authorized token.json if any
  if token_json_f.exists():
    creds = Credentials.from_authorized_user_file(token_json_f.as_posix(), SCOPES)
  else:
    creds = None
  #endregion try previous authorized token.json if any

  # prev token not valid / notvalid / expired
  if not creds or not creds.valid:
    # try refresh the token
    if creds and creds.expired and creds.refresh_token:
      creds.refresh(Request())

    # all failed for prev login, , let the user log in again > log in w/ :credentials_json_f aka gcp client secret json
    else:
      flow  = InstalledAppFlow.from_client_secrets_file(clientsecret_json_f.as_posix(), SCOPES)
      creds = flow.run_local_server(port=0)

    # save the credentials for the next run
    with open(token_json_f.as_posix(), 'w') as f: f.write(creds.to_json())

  return creds


#region google spreadsheet aka ggshet
def read_range(sheet_id, cell_range, credentials):
  service = build('sheets', 'v4', credentials=credentials)
  ggsheet = service.spreadsheets()

  result = ggsheet.values().get(
    spreadsheetId = sheet_id,
    range         = cell_range,
  ).execute()

  values = result.get('values', [])
  return values


def write_range(values,  sheet_id, cell_range, credentials):
  service = build('sheets', 'v4', credentials=credentials)

  result  = service.spreadsheets().values().update(  # ref https://github.com/googleworkspace/python-samples/blob/main/sheets/snippets/sheets_batch_update_values.py
    spreadsheetId    = sheet_id,
    range            = cell_range,
    valueInputOption = 'USER_ENTERED',
    body             = {'values': values},
  ).execute()

  '''
  you may get error 
  > googleapiclient.errors.HttpError: <HttpError 403 when requesting https://sheets.googleapis.com/v4/spreadsheets/12RoMSEtnY7Xlq1i510mWRrQuyECiZF7PCwNUCTPdYag/values/Sheet%202%21B%3AC?valueInputOption=USER_ENTERED&alt=json returned "Request had insufficient authentication scopes.". Details: "[{'@type': 'type.googleapis.com/google.rpc.ErrorInfo', 'reason': 'ACCESS_TOKEN_SCOPE_INSUFFICIENT', 'domain': 'googleapis.com', 'metadata': {'method': 'google.apps.sheets.v4.SpreadsheetsService.UpdateValues', 'service': 'sheets.googleapis.com'}}]">
  
  fix the scope  ref https://stackoverflow.com/a/74582568/248616
  all scopes     ref https://developers.google.com/sheets/api/scopes
  '''
  return result
#endregion google spreadsheet aka ggshet


##region google drive

def get_ggdrive_file(ggdrive, ggdriveitem_id):
  resp_d  = ggdrive.files().get(fileId=ggdriveitem_id, fields='name,mimeType').execute()  # raise TypeError('Missing required parameter "%s"' % name)
  return resp_d


def list_ggdrive_folder(folder_id, credentials):
  """
  python to download file fr google drive
  ref https://stackoverflow.com/a/76736234/248616
  ref https://stackoverflow.com/q/73119251/248616
  """
  ggdrive = build('drive', 'v3', credentials=credentials)

  '';                              query=f"'{folder_id}' in parents"
  results = ggdrive.files().list(q=query, pageSize=1000).execute()
  items = results.get('files', [])
  return items


def download_file(ggdrive, file_id, to_p, depth, verbose=False):
  resp_d       = get_ggdrive_file(ggdrive, file_id)
  filename     = resp_d.get('name'); assert filename, f'Cannot load file at {file_id=:<}'
  local_file_p = to_p/filename

  #region do download
  request    = ggdrive.files().get_media(fileId=file_id)
  fd         = io.BytesIO()
  downloader = MediaIoBaseDownload(fd, request)

  done = False
  while not done:
    _, done = downloader.next_chunk()

  local_file_p.parent.mkdir(parents=True, exist_ok=True)

  # local_file_p.write_binary(fd.getvalue())  #TODO why some file get err  > AttributeError: 'PosixPath' object has no attribute 'write_binary'
  with open(local_file_p.as_posix(), 'wb') as f:
    f.write(fd.getvalue())
  #endregion do download

  if verbose: print(f'''{('-'*depth):<22} {filename:<22} downloaded {local_file_p}''')

  assert local_file_p.exists()
  return local_file_p

###

class FileType:
  FOLDER    = 'application/vnd.google-apps.folder'
  FILE      = 'application/vnd.google-apps.file'
  SHORTCUT  = 'application/vnd.google-apps.shortcut'


def get_ggdrive_filetype(ggdrive, ggdriveitem_id):
  """
  ref gg gemini https://g.co/gemini/share/2b52884a68f7
  """
  resp_d  = ggdrive.files().get(fileId=ggdriveitem_id, fields='mimeType').execute()
  return resp_d.get('mimeType')


def get_original_file_id(ggdrive, shortcut_id):
  """
  ref gg gemini https://g.co/gemini/share/12ca80735a4e
  """
  from googleapiclient.errors import HttpError

  try:
    resp_d = ggdrive.files().get(fileId=shortcut_id, fields='shortcutDetails').execute()
  except HttpError as error:
    print("Error getting shortcut details:", error)
    return None

  if 'shortcutDetails' in resp_d:
    return resp_d['shortcutDetails']['targetId']
  else:
    print("Shortcut details not found for:", shortcut_id)
    return None


def download_fr_ggdrive(ggdriveitem_id, to_p, credentials, verbose=False):
  '';                  ggdrive = build('drive', 'v3', credentials=credentials)
  _download_fr_ggdrive(ggdrive, ggdriveitem_id, to_p, parent_p=None,depth=0, verbose=verbose)


def _download_fr_ggdrive(ggdrive, ggdriveitem_id, to_p,
                         parent_p, depth,
                         verbose=False,
):
  item = get_ggdrive_file(ggdrive, ggdriveitem_id)
  parent_s = parent_p.name if parent_p else ''
  if verbose: print(f'''{('-'*depth + parent_s):<22} {item['name']:<22} {item['mimeType']} ... ''')

  filetype = get_ggdrive_filetype(ggdrive, ggdriveitem_id)
  if filetype == FileType.FOLDER:
    '''recursively download a folder'''
    resp_d = ggdrive.files().list(q=f"'{ggdriveitem_id}' in parents and trashed=false").execute()
    items  = resp_d.get('files', [])
    for item in items:
      _download_fr_ggdrive(ggdrive, item['id'], to_p=to_p/item['name'],
                           parent_p=to_p, depth=depth+1,
                           verbose=verbose)

  elif filetype == FileType.SHORTCUT:
    '';                           origin_id = get_original_file_id(ggdrive, shortcut_id=ggdriveitem_id); assert origin_id
    _download_fr_ggdrive(ggdrive, origin_id, to_p=to_p,
                         parent_p=to_p, depth=depth+1,
                         verbose=verbose)

  # elif filetype == FileType.FILE:  # files has multiple types beside vnd.google-apps.file eg application/octet-stream
  else:
    #TODO faster by calling download_file(ggdrive, item, ...) insteadof download_file(ggdrive, ggdriveitem_id, ...)
    download_file(ggdrive, ggdriveitem_id,
                  to_p.parent, depth,
                  verbose=verbose)

##endregion google drive
