"""
ref https://github.com/googleworkspace/python-samples/blob/main/sheets/quickstart/quickstart.py
"""
from python_consoleapp.src.service.google_client._ import do_auth_n_get_tokenfile, read_range



#region sample
sample_vault = {
  'sample1': dict(
    SAMPLE_SPREADSHEET_ID = '1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms',
    SAMPLE_RANGE_NAME     = 'Class Data!A2:E',
  ),

  'sample2': dict(
    SAMPLE_SPREADSHEET_ID = '12RoMSEtnY7Xlq1i510mWRrQuyECiZF7PCwNUCTPdYag',
    SAMPLE_RANGE_NAME     = 'Sheet 2!B:C',
  ),
}

SAMPLE_SPREADSHEET_ID = sample_vault['sample2']['SAMPLE_SPREADSHEET_ID']
SAMPLE_RANGE_NAME     = sample_vault['sample2']['SAMPLE_RANGE_NAME']
#endregion sample

values  = read_range(sheet_id=SAMPLE_SPREADSHEET_ID, cell_range=SAMPLE_RANGE_NAME, credentials=do_auth_n_get_tokenfile() )
print('\n'.join([str(i) for i in values]) )
