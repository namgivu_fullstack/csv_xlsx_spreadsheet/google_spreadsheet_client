--- get started
gg python google spreadsheet read
https://developers.google.com/sheets/api/quickstart/python

get gcp oauth client secret

python3 -m pipenv install \
  google-api-python-client \
  google-auth-httplib2 \
  google-auth-oauthlib


--- ensure you enable the api(s) of the google app
> https://console.cloud.google.com/apis/library/browse?filter=category:gsuite&project=plexiform-bot-401206
>       //                                            ?                       project=your project id

eg
gmail 
drive 
form 
spreadsheet
